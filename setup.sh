#!/bin/env sh
#
# setup.sh: install configuration files into current user's environment.
# Author: Sudaraka Wijesinghe
#

echo 'Dotfiles Setup.'
echo '==============='
echo 'Copyright 2013-2021 Sudaraka Wijesinghe. <sudaraka@sudaraka.org>'
echo 'Creative Commons Attribution 3.0 Unported License.'
echo

DOTFILES_DIR=$(realpath "$(dirname "$0")");

if [ -z "$DOTFILES_DIR" ]; then
  echo 'Unable to determine dotfiles repository.';
  echo 'Check if "realpath" package is missing in your system.'
  exit 1;
fi;

# Vim configuration {{{

echo 'Setup Vim Configuration'
echo

# Remove existing configuration and recreate directories
rm -fr ~/.config/vimrc 2>/dev/null
rm -fr ~/.config/vim 2>/dev/null
rm -f ~/.editorconfig 2>/dev/null

ln -sv "$DOTFILES_DIR/vim" ~/.config/vim
ln -sv .config/vim/editorconfig ~/.editorconfig

# Make vim use aspell dictionary
mkdir -pv ~/.config/vim/spell
rm ~/.config/vim/spell/en.utf-8.add
ln -sv ~/.aspell.en.pws ~/.config/vim/spell/en.utf-8.add

# Install submodules (Vim plugins)
git \
  --git-dir="$DOTFILES_DIR/.git/" \
  --work-tree="$DOTFILES_DIR/" \
  submodule update \
  --init \
  --remote

rm "$DOTFILES_DIR/vim/pack/plugin/start/*"

find "$DOTFILES_DIR/vim/repo" \
  -mindepth 1 -maxdepth 1 -type d \
  -exec ln -s {} vim/pack/plugin/start/ \;

vim +CocUpdate +qa

echo

# }}}

# Bash configuration {{{

echo 'Setup Bash Configuration'
echo

for BASH_SCRIPT in bash_profile bashrc bash_completion; do
  BASH_SCRIPT_PATH="$DOTFILES_DIR/bash/$BASH_SCRIPT"

  [ ! -f "$BASH_SCRIPT_PATH" ] && continue;

  BASH_SCRIPT_FILE=$(basename "$BASH_SCRIPT_PATH")
  BASH_SCRIPT_TARGET="$HOME/.$BASH_SCRIPT_FILE"

  rm -f "$BASH_SCRIPT_TARGET"
  ln -sv "$BASH_SCRIPT_PATH" "$BASH_SCRIPT_TARGET"
done;

rm -f ~/.config/starship.toml 2>/dev/null
ln -sv "$DOTFILES_DIR/bash/starship.toml" ~/.config/starship.toml

BASH_COMPONENETS="
10-localbin
20-localopt
"
for COMPONENT in $BASH_COMPONENETS; do
  SOURCE="../available/$(echo "$COMPONENT" | sed 's/[0-9]\+-//').sh"
  TARGET="$DOTFILES_DIR/bash/components/enabled/$COMPONENT"

  [ -e "$TARGET" ] && rm "$TARGET"
  ln -sv "$SOURCE" "$TARGET"
done

rm -fr "$HOME/.local/share/bash-components"
ln -sv "$DOTFILES_DIR/bash/components/enabled" "$HOME/.local/share/bash-components"

rm -f ~/.signature 2>/dev/null
ln -sv "$DOTFILES_DIR/signature" ~/.signature

echo

# }}}

# Git configuration {{{

echo 'Setup Git Configuration'
echo

# Remove existing configuration
if [ -L ~/.config/git ]; then
  rm -f ~/.config/git 2>/dev/null
else
  rm -fr ~/.config/git/ 2>/dev/null
fi

ln -sv "$DOTFILES_DIR/git" ~/.config/git

echo

# }}}

# GUI configuration {{{

echo 'Setup GUI Configuration'
echo

# Remove existing configuration
rm -f ~/.xinitrc 2>/dev/null
rm -f ~/.Xmodmap 2>/dev/null
rm -fr ~/.config/gtk-3.0 2>/dev/null
rm -fr ~/.config/gtk-4.0 2>/dev/null
rm -f ~/.config/Trolltech.conf 2>/dev/null
rm -fr ~/.config/i3 2>/dev/null
rm -fr ~/.config/dunst 2>/dev/null
rm -fr ~/.config/tilda 2>/dev/null
rm -fr ~/.config/rofi 2>/dev/null

ln -sv "$DOTFILES_DIR/xinitrc" ~/.xinitrc
ln -sv "$DOTFILES_DIR/ui/Trolltech.conf" ~/.config/
ln -sv "$DOTFILES_DIR/ui/gtk-3.0" ~/.config/
# use GTK v3 config as v4 config
ln -sv "$DOTFILES_DIR/ui/gtk-3.0" ~/.config/gtk-4.0

if [ -f "$DOTFILES_DIR/Xmodmap.$(uname -n)" ]; then
  ln -sv "$DOTFILES_DIR/Xmodmap.$(uname -n)" ~/.Xmodmap
else
  ln -sv "$DOTFILES_DIR/Xmodmap" ~/.Xmodmap
fi

# setup i3wm config and scripts
PRIMARY_DISPLAY=$(xrandr|grep '\sconnected'|cut -d' ' -f1|head -n1);
SECONDARY_DISPLAY=$(xrandr|grep 'HDMI'|cut -d' ' -f1|head -n1);
WIRELESS_INTERFACE=$(ip link|grep ': wl'|cut -d':' -f2|tr -d ' ');
ETHERNET_INTERFACE=$(ip link|grep ': en'|cut -d':' -f2|tr -d ' ');
BATTERY_ID=$(find /sys/class/power_supply -type l -name 'BAT*' -print0|xargs basename)
FONT_SIZE=14

if [ -z "$WIRELESS_INTERFACE" ]; then
  WIRELESS_INTERFACE="wlnx0"
fi;

# for file in config; do
file="config"
  rm "$DOTFILES_DIR/ui/i3/$file"
  cp -v "$DOTFILES_DIR/ui/i3/$file.default" "$DOTFILES_DIR/ui/i3/$file"

  sed "s/%PRIMARY_DISPLAY%/$PRIMARY_DISPLAY/" \
    -i "$DOTFILES_DIR/ui/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%SECONDARY_DISPLAY%/$SECONDARY_DISPLAY/" \
    -i "$DOTFILES_DIR/ui/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%VERTICAL_DISPLAY%/$PRIMARY_DISPLAY/" \
    -i "$DOTFILES_DIR/ui/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%WIRELESS_INTERFACE%/$WIRELESS_INTERFACE/" \
    -i "$DOTFILES_DIR/ui/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%ETHERNET_INTERFACE%/$ETHERNET_INTERFACE/" \
    -i "$DOTFILES_DIR/ui/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%BATTERY_ID%/$BATTERY_ID/" \
    -i "$DOTFILES_DIR/ui/i3/$file" \
    >/dev/null 2>&1;

  sed "s/%FONT_SIZE%/$FONT_SIZE/" \
    -i "$DOTFILES_DIR/ui/i3/$file" \
    >/dev/null 2>&1;
# done;

ln -sv "$DOTFILES_DIR/ui/i3" ~/.config
ln -sv "$DOTFILES_DIR/ui/i3status" ~/.config/i3status-rust
ln -sv "$DOTFILES_DIR/ui/dunst" ~/.config
ln -sv "$DOTFILES_DIR/ui/tilda" ~/.config
ln -sv "$DOTFILES_DIR/ui/rofi" ~/.config

mkdir -p ~/.local/bin >/dev/null 2>&1
rm ~/.local/bin/i3exit >/dev/null 2>&1
ln -sv "$DOTFILES_DIR/ui/i3/i3exit" ~/.local/bin

echo

# }}}

# Dictionary file (aspell) {{{

echo 'Setup Dictionary for Aspell'
echo

# Remove existing configuration
rm -f ~/.aspell.en.pws 2>/dev/null

ln -sv "$DOTFILES_DIR/aspell.en.pws" ~/.aspell.en.pws

echo

# }}}

# Enable startup system services {{{

echo 'Enable Startup system Services'
echo

for service in "$DOTFILES_DIR/startup"/*.service; do
  echo " - $(basename "${service%.*}")"

  systemctl --user enable "$service" >/dev/null 2>&1
done

systemctl --user daemon-reload >/dev/null

# }}}

# Npm configuration {{{

echo 'Setup Npm Configuration'
echo

# Remove existing configuration
rm -f ~/.npmrc 2>/dev/null

ln -sv "$DOTFILES_DIR/npmrc" ~/.npmrc

echo

# }}}

# Firefox custom/user preferences {{{

FFPROFILEDIR=$(find "$HOME/.mozilla/firefox" -maxdepth 1 -type d -name '*.default' | head -n1)

if [ -n "$FFPROFILEDIR" ]; then
  echo 'Setup Firefox custom/user preferences'
  echo

  # Remove existing configuration
  rm -f "$FFPROFILEDIR/user.js" 2>/dev/null

  ln -sv "$DOTFILES_DIR/firefox/user.js" "$FFPROFILEDIR"

  echo
fi

# }}}

# Rust/Cargo configuration {{{

echo 'Setup Rust/Cargo configuration'
echo

# Remove existing configuration
rm -f "$HOME/rustfmt.toml" 2>/dev/null
rm -f "$HOME/.cargo/config" 2>/dev/null
rm -f "$HOME/.cargo/config.toml" 2>/dev/null

ln -sv "$DOTFILES_DIR/rust/rustfmt.toml" "$HOME/rustfmt.toml"

mkdir -p "$HOME/.cargo"
ln -sv "$DOTFILES_DIR/rust/cargo-config.toml" "$HOME/.cargo/config.toml"

echo

# }}}

# Zellij configuration {{{

echo 'Setup Fzf Configuration'
echo

# Remove existing configuration
if [ -L ~/.local/share/fzf ]; then
  rm -f ~/.local/share/fzf 2>/dev/null
else
  rm -fr ~/.local/share/fzf/ 2>/dev/null
fi

ln -sv "$DOTFILES_DIR/fzf" ~/.local/share/fzf

# }}}

# Zellij configuration {{{

echo 'Setup Zellij Configuration'
echo

# Remove existing configuration
if [ -L ~/.config/zellij ]; then
  rm -f ~/.config/zellij 2>/dev/null
else
  rm -fr ~/.config/zellij/ 2>/dev/null
fi

ln -sv "$DOTFILES_DIR/zellij" ~/.config/zellij

# }}}
