" vim/ftplugin/sql.vim: SQL file type specific configuration
"
" Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
"
" This program comes with ABSOLUTELY NO WARRANTY;
" This is free software, and you are welcome to redistribute it and/or modify it
" under the terms of the BSD 2-clause License. See the LICENSE file for more
" details.
"

" Autoformat on save
augroup format_with_coc
  autocmd!
  autocmd BufWritePre *.sql call CocAction("format")
augroup END

" abbreviation for file header with BSD licensed code
iabbrev head! -- <esc>"%pA<CR>
      \<CR>
      \Copyright <C-R>=strftime('%Y')<CR> Sudaraka Wijesinghe <sudaraka@sudaraka.org><CR>
      \<CR>
      \This program comes with ABSOLUTELY NO WARRANTY;<CR>
      \This is free software, and you are welcome to redistribute it and/or modify<CR>
      \it under the terms of the BSD 2-clause License. See the LICENSE file for<CR>
      \mote details.<CR>
      \<CR><ESC>0d$o<ESC>

" abbreviation for file header with proprietary code
iabbrev phead! -- <esc>"%pA<CR>
      \<CR>
      \Copyright <C-R>=strftime('%Y')<CR> {enter client name here}. All rights reserved.
      \<CR>
      \Author  : Sudaraka Wijesinghe <sudaraka@sudaraka.org><CR>
      \Created : <C-R>=strftime('%Y-%m-%d')<CR>
      \<CR>
      \<CR><ESC>0d$o<ESC>
