" vim/ftplugin/html.vim: HTML file type specific configuration
"
" Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
"
" This program comes with ABSOLUTELY NO WARRANTY;
" This is free software, and you are welcome to redistribute it and/or modify it
" under the terms of the BSD 2-clause License. See the LICENSE file for more
" details.
"

" abbreviation for HTML5 template
iabbrev tmpl! <!doctype html>
                  \<CR>
                  \<html lang="en">
                  \<CR>
                  \<head>
                  \<CR>
                  \<meta charset="utf-8">
                  \<CR>
                  \<meta name="viewport" content="width=device-width, initial-scale=1">
                  \<CR>
                  \<title></title>
                  \<CR>
                  \</head>
                  \<CR>
                  \<body>
                  \<CR>
                  \</body>
                  \<CR>
                  \</html>
                  \<ESC>kO
