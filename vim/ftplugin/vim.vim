" vim/ftplugin/vim.vim: Vim script specific configuration
"
" Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
"
" This program comes with ABSOLUTELY NO WARRANTY;
" This is free software, and you are welcome to redistribute it and/or modify
" it under the terms of the BSD 2-clause License. See the LICENSE file for
" mote details.
"

" abbreviation for file header with BSD licensed code
iabbrev head! " <esc>"%pA:<CR>
      \<CR>
      \Copyright <C-R>=strftime('%Y')<CR> Sudaraka Wijesinghe <sudaraka@sudaraka.org><CR>
      \<CR>
      \This program comes with ABSOLUTELY NO WARRANTY;<CR>
      \This is free software, and you are welcome to redistribute it and/or modify<CR>
      \it under the terms of the BSD 2-clause License. See the LICENSE file for<CR>
      \mote details.<CR>
      \<CR>
      \<ESC>ggA
