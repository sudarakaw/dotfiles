" vim/ftplugin/php.vim: PHP file type specific configuration
"
" Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
"
" This program comes with ABSOLUTELY NO WARRANTY;
" This is free software, and you are welcome to redistribute it and/or modify
" it under the terms of the BSD 2-clause License. See the LICENSE file for
" mote details.
"
"

" abbreviation for file header with BSD licensed code
iabbrev head! <?php<CR>
      \/** <esc>"%pA:<CR>
      \<CR>
      \@copyright <C-R>=strftime('%Y')<CR> Sudaraka Wijesinghe <sudaraka@sudaraka.org><CR>
      \@license BSD-2-Clause<CR>
      \<CR>
      \This program comes with ABSOLUTELY NO WARRANTY;<CR>
      \This is free software, and you are welcome to redistribute it and/or modify<CR>
      \it under the terms of the BSD 2-clause License. See the LICENSE file for<CR>
      \mote details.<CR>
      \<CR>
      \/<ESC>ggA

" abbreviation for file header with proprietary code
iabbrev phead! <?php<CR>
      \/** <esc>"%pA:<CR>
      \<CR>
      \@copyright <C-R>=strftime('%Y')<CR> {enter client name here}. All rights reserved.
      \<CR>
      \@author Sudaraka Wijesinghe <sudaraka@sudaraka.org><CR>
      \Created: <C-R>=strftime('%Y-%m-%d')<CR>
      \<CR>
      \<CR>
      \/<ESC>ggA
