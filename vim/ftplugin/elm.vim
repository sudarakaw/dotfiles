" vim/ftplugin/elm.vim: Elm file type specific configuration
"
" Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
"
" This program comes with ABSOLUTELY NO WARRANTY;
" This is free software, and you are welcome to redistribute it and/or modify it
" under the terms of the BSD 2-clause License. See the LICENSE file for more
" details.
"

" Autoformat on save
augroup format_with_coc
  autocmd!
  autocmd BufWritePre *.elm call CocAction("format")
augroup END
