" vim/ftplugin/rust.vim: Rust file type specific configuration
"
" Copyright 2021 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
"
" This program comes with ABSOLUTELY NO WARRANTY;
" This is free software, and you are welcome to redistribute it and/or modify it
" under the terms of the BSD 2-clause License. See the LICENSE file for more
" details.
"

" Autoformat on save
augroup format_with_coc
  autocmd!
  autocmd BufWritePre *.rs call CocAction("format")
augroup END

" Key binding to reload Rust Analyzer via Coc.[n]vim
nnoremap <leader>crr :CocCommand rust-analyzer.reload<CR>

" abbreviation for file header with BSD licensed code
iabbrev head! /* <esc>"%pA:<CR>
      \<CR>
      \Copyright <C-R>=strftime('%Y')<CR> Sudaraka Wijesinghe <sudaraka@sudaraka.org><CR>
      \<CR>
      \This program comes with ABSOLUTELY NO WARRANTY; This is free software, and<CR>
      \you are welcome to redistribute it and/or modify it under the terms of the<CR>
      \BSD 2-clause License. See the LICENSE file for more details.<CR>
      \<CR>
      \/<ESC>ggA

" abbreviation for file header with proprietary code
iabbrev phead! /* <esc>"%pA:<CR>
      \<CR>
      \Copyright <C-R>=strftime('%Y')<CR> {enter client name here}. All rights reserved.
      \<CR>
      \Author  : Sudaraka Wijesinghe <sudaraka@sudaraka.org><CR>
      \Created : <C-R>=strftime('%Y-%m-%d')<CR>
      \<CR>
      \<CR>
      \/<ESC>ggA

iabbrev tmplmoderr! pub type Result<T> = core::result::Result<T, Error>;<CR>
    \<CR>
    \#[derive(Debug)]<CR>
    \pub enum Error {}<CR>
    \<CR>
    \//impl From<module::Error> for Error {<CR>
    \<ESC>a     fn from(value: module::Error) -> Self {<CR>
    \<ESC>a         Self::Variant(value)<CR>
    \<ESC>a     }<CR>
    \<ESC>a}<CR>
    \<ESC>0d$o
    \impl std::fmt::Display for Error {<CR>
    \fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {<CR>
    \write!(f, "{self:?}")<CR>
    \}<CR>
    \}<CR>
    \<CR>
    \impl std::error::Error for Error {}
