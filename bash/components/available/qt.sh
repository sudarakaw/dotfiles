# bash/components/available/qt.sh:
#

# Qt Creator/Framework paths
QT_PATH="$HOME/Qt"

if [ -d "$QT_PATH" ]; then
  QT_BINDIRS=$(find "$QT_PATH" -maxdepth 3 -type d -name bin | sort -r | xargs)
  COMPONENT_PATHS="$COMPONENT_PATHS $QT_BINDIRS"
fi;

