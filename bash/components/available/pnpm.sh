# bash/components/available/pnpm.sh:
#

# pnpm paths
export PNPM_HOME="$HOME/.local/share/pnpm"

# shellcheck disable=2034
COMPONENT_PATHS="$COMPONENT_PATHS $PNPM_HOME ./node_modules/.bin"
