# bash/components/available/android.sh:
#

# Android SDK root
ANDROID_HOME="$HOME/opt/android-sdk"

if [ -d "$ANDROID_HOME" ]; then
  export ANDROID_HOME

  BINDIRS=""

  # Android commandline tools (sdkmanager)
  BINDIRS="$BINDIRS $ANDROID_HOME/cmdline-tools/latest/bin"

  # Android platform tools
  BINDIRS="$BINDIRS $ANDROID_HOME/platform-tools"

  # NDK
  NDK_HOME="$ANDROID_HOME/ndk/$(/bin/ls -1 "$ANDROID_HOME/ndk")"
  export NDK_HOME

  # Android Emulator
  BINDIRS="$BINDIRS $ANDROID_HOME/emulator"

  COMPONENT_PATHS="$COMPONENT_PATHS $BINDIRS"
fi
