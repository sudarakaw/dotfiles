#!/usr/bin/env sh
#
# clean-hist.sh: remove duplicate & predefined entry patterns from bash history
#
# IMPORTANT: in order to remove duplicates via `uniq` command, history list is
# being sorted. This will make the resulting history NOT in the chronological
# order.
#
# Copyright 2022 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY;
# This is free software, and you are welcome to redistribute it and/or modify it
# under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

if [ ! -f "$HISTFILE" ]; then
  echo "Unable to find history file. Try adding following line to ~/.bashrc"
  echo
  echo "  export HISTFILE"
  echo

  exit 1
fi

TMPFILE=$(mktemp)
SINGLE_WORD_COMMANDS="cargo|cd|cpuinfo|df|exit|gpodder|lspci|ls|lspci|lsusb|make|mount|pacman|paru|pwd|root|set|steam|vim|webpack|xrandr|zellij"

sort -h "$HISTFILE" \
  | sed 's/[ \t]*$//' \
  | grep -Ev '^[#-\"''].*' \
  | grep -Ev '^.*--(help|version)' \
  | grep -Ev '^.*-(h|v)\s*$' \
  | grep -Ev '^.?$' \
  | grep -Ev "^(${SINGLE_WORD_COMMANDS}${_PROJECT_NAMES})\s*$" \
  | grep -Ev "^[A-Z_]+\s*=?\s*" \
  | grep -Ev '^adb\s*$' \
  | grep -Ev '^adb\s+(devices|shell)' \
  | grep -Ev '^a[gh]\s+' \
  | grep -Ev '^alias\s*' \
  | grep -Ev '^autotiling\s*' \
  | grep -Ev '^basename\s+' \
  | grep -Ev '^borg\s+(compact|delete|export.*|info|init|list|mount|prune)' \
  | grep -Ev '^caddy\s+' \
  | grep -Ev '^cal\s*' \
  | grep -Ev '^cargo\s+' \
  | grep -Ev '^[z]*ca[rt]\s+' \
  | grep -Ev '^cd\s+' \
  | grep -Ev '^chmod\s+' \
  | grep -Ev '^chown\s+' \
  | grep -Ev '^\./clean-hist\.sh' \
  | grep -Ev '^composer\s+' \
  | grep -Ev '^cp\s*' \
  | grep -Ev '^curl\s+[^-]' \
  | grep -Ev '^curl\s+-[iv]' \
  | grep -Ev '^date\s+' \
  | grep -Ev '^df\s+' \
  | grep -Ev '^diff\s+' \
  | grep -Ev '^dig\s+' \
  | grep -Ev '^dirname\s*' \
  | grep -Ev '^docker\s+(build|cp|exec|image|kill|ps|restart|rm|run|search|volume)' \
  | grep -Ev '^du\s+' \
  | grep -Ev '^echo\s+' \
  | grep -Ev '^elm-?\s*' \
  | grep -Ev '^export\s+' \
  | grep -Ev '^file\s+' \
  | grep -Ev '^find\s+' \
  | grep -Ev '^f\s+' \
  | grep -Ev '^git\s+' \
  | grep -Ev 'GOG\\ Games' \
  | grep -Ev '^gpg[2]?\s+' \
  | grep -Ev '^g\s+' \
  | grep -Ev '^grep\s+' \
  | grep -Ev '^head\s+' \
  | grep -Ev '^hexdump\s+' \
  | grep -Ev '^history\s+' \
  | grep -Ev '^ip\s+' \
  | grep -Ev '^journalctl\s+' \
  | grep -Ev '^ldd\s+' \
  | grep -Ev '^ln\s+' \
  | grep -Ev '^ls(.{3})?\s*' \
  | grep -Ev '^makepkg\s*' \
  | grep -Ev '^man\s+' \
  | grep -Ev '^magick\s*' \
  | grep -Ev '^mcd\s+' \
  | grep -Ev '^(sudo\s+)?meld\s*' \
  | grep -Ev '^mkdir\s+' \
  | grep -Ev '^mv\s+' \
  | grep -Ev '^modprobe\s*' \
  | grep -Ev '^namcap\s+' \
  | grep -Ev '^nc\s+' \
  | grep -Ev '^node\s+' \
  | grep -Ev '^notify-send\s+' \
  | grep -Ev '^npm\s*' \
  | grep -Ev '^n\s+(outdated|update|why|list|test|i\s?)' \
  | grep -Ev '^paru\s+[^-].*' \
  | grep -Ev '^paru\s+-S[s]*\s+' \
  | grep -Ev '^paru\s+-[^Q]' \
  | grep -Ev '^paru\s+-Q[^t]' \
  | grep -Ev '^php\s+' \
  | grep -Ev '^ping\s+' \
  | grep -Ev '^(sudo\s+)?pkgfile\s+' \
  | grep -Ev '^pkill\s+' \
  | grep -Ev '^pnpm\s*' \
  | grep -Ev '^ps\s*' \
  | grep -Ev '^rmdir\s+' \
  | grep -Ev '^rm\s+' \
  | grep -Ev '^rustup\s*' \
  | grep -Ev '^scan2pdf\s*' \
  | grep -Ev '^scp\s+' \
  | grep -Ev '^screen\s+' \
  | grep -Ev '^sed\s+' \
  | grep -Ev '^sha[0-9]*sum\s*' \
  | grep -Ev '^size\s*' \
  | grep -Ev '^sqlite.?\s*' \
  | grep -Ev '^ssh-(add|keygen)\s+' \
  | grep -Ev '^starship\s*' \
  | grep -Ev '^strings\s+' \
  | grep -Ev '^strip\s+' \
  | grep -Ev '^(sudo\s+)?pacman\s+-[^Q]' \
  | grep -Ev '^(sudo\s+)?pacman\s+-Q[^t]' \
  | grep -Ev '^tail\s+' \
  | grep -Ev '^target\\*' \
  | grep -Ev '^tar\s+' \
  | grep -Ev '^tee\s+' \
  | grep -Ev '^touch\s+' \
  | grep -Ev '^(b|h|nv)?top\s*' \
  | grep -Ev '^tree\s*' \
  | grep -Ev '^tre\s*' \
  | grep -Ev '^tune2fs\s*' \
  | grep -Ev '^uname\s*' \
  | grep -Ev '^vim\s+([^\+~])' \
  | grep -Ev '^v\s+' \
  | grep -Ev '^watch\s+' \
  | grep -Ev '^which\s+' \
  | grep -Ev '^xev\s*' \
  | grep -Ev '^xprop\s*' \
  | grep -Ev '^xrandr\s*' \
  | grep -Ev '^yk(chal|man)\s*' \
  | grep -Ev '^yt-dlp\s+' \
  | grep -Ev '^z[eir]\s+' \
  | grep -Ev '^zri\s+' \
  | uniq \
  1> "$TMPFILE"

# backup existing history file before overrating.
mv "$HISTFILE" "${HISTFILE}-$(date '+%Y%m%d%H%M%S')"

mv "$TMPFILE" "$HISTFILE"
# echo "-- 8< ------"
# cat "$TMPFILE"
