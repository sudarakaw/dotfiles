# dotfiles

Collection of configuration files and script I use on my workstations.

**Warning:** included setup script is (setup.sh) will wipe out your existing
configuration.

Copyright 2013 [Sudaraka Wijesinghe] [url_contact].

This work is licensed under the Creative Commons Attribution 3.0 Unported
License. To view a copy of this license, visit
[http://creativecommons.org/licenses/by/3.0/] [url_cc].

## Used with

**OS:** Archlinux

**Shell:** Bash

**Desktop:** i3wm

  [url_contact]: https://sudaraka.org/contact/
  [url_cc]: http://creativecommons.org/licenses/by/3.0/
